<?php
/**
 * function definition for the getRecommendation
 */
function getRecommendation( $email, $usernames ){

  $basename = strtolower( explode('@', $email)[0] );
  $basename = rtrim( $basename, ' 0123456789');
  $suffix = '';
  $nextTail = 0;
  $assigned = false;
  $recordCount = count( $usernames );

  foreach ( $usernames as $key => $username ){
     if ( stristr( $username, $basename ) !== FALSE ) {
       $tail = str_replace( $basename, '' , trim($username) );
       if( ctype_digit( $tail ) ){
          $nextTail = (int) $suffix + 1;
          if ( $nextTail < $tail ) {
            $suffix = $nextTail;
            $assigned = true;
            break;
          }else{
            $suffix = $tail;
          }
       }
     }
  }

  if( !$assigned && $suffix != '' ) {
    ++$suffix;
  }
  return $basename . $suffix;
}
