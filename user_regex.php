<?php
/**
 * Opens the text file as a string and uses regex to get an array of all
 * matching names.
 *
 * Assumption:
 *   - if the name is found, the highest suffix value is incremented is used for the
 *     recommendation
 */

// get first parameter passed from commandline as email to be processed
require('recommendation.php');
$email = $argv[1];
echo "\n Starting process... \n\n";

$start  = microtime(true);

$file = file_get_contents( 'usernames.txt' );
$basename = strtolower( explode('@', $email)[0] );
$basename = rtrim( $basename, ' 0123456789');
$pattern = '/(\s' . $basename . '([0-9]*))/';
preg_match_all( $pattern, $file, $matches, PREG_PATTERN_ORDER );

$index = max(array_unique( $matches[2] ) );
$index = ( $index === '' ) ? '' : ++$index;
$recommendation = $basename . $index;

$end = microtime(true);
$lapsedTime = $end - $start;
echo "\nRecommended Username: $recommendation";
echo "\n\nProcessing Time: " . $lapsedTime;
echo "\n\n";
