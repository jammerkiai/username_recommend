<?php
/**
 * Gets a username recommendation from the commandline using
 * an object-oriented approach.
 *
 * This approach makes use of the sqlite database, therefore the database has to be
 * created first if it is not yet present.
 *
 * To set up the database, on the commandline type:
 *   php user_oop.php dbsetup
 *
 * This can take a while to complete as the data is automatically indexed.
 *
 * After db setup is complete, the recommendation script can now be used.
 *
 * Syntax:
 *   php user_oop.php <email>
 *
 * Sample:
 *   php user_oop.php jammer@email.com
 *
 *
 */

require('UsernameModel.php');

// get first parameter passed from commandline as email to be processed
$param = $argv[1];
echo "\n Starting process... \n\n";

$start  = microtime(true);
$username = new Username();

if( $param === 'dbsetup' ){
  $username->setupDb();
} else {

  $recommendation = $username->recommend( $param );
  echo "\nRecommended Username: $recommendation";
}

$end = microtime(true);
$lapsedTime = $end - $start;
echo "\n\nProcessing Time: " . $lapsedTime;
echo "\n\n";
