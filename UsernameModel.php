<?php
/**
 * Username Model
 *
 * Encapsulates code for creating/accessing Sqlite via PDO
 *
 */

class Username {
  protected $_srcFile = 'usernames.txt';
  protected $_usernames = array();
  protected $_db;

  public function __construct() {
    try {
      $this->_db = new PDO('sqlite:usernameDB.sq3');
    }catch(Exception $e){
      die('Could not connect to the database. Please verify that the DB is correctly set up.');
    }
  }

  public function setSrcFile( $file ) {
    $this->_srcFile = $file;
  }

  protected function _getUsersFromSource(){
    ini_set( 'memory_limit' , -1);
    $this->_usernames = file( $this->_srcFile );

    $usernames = array();
    foreach( $this->_usernames as $name ){
      array_push( $usernames, array( 'username'=> trim($name) ) );
    }

    return $usernames;
  }

  public function setupDb(){
    $usernames = $this->_getUsersFromSource();

    $this->_db->exec( "BEGIN TRANSACTION" );

    $sql = "CREATE TABLE usernames(username TEXT PRIMARY KEY, base TEXT NOT NULL, suffix INT NOT NULL);";
    $sql .= "CREATE INDEX user_idx1 on  usernames( base , suffix );";
    $this->_db->exec( $sql );

    $recordTotal = count($usernames);
    try{
      $insert = "INSERT INTO usernames (username, base, suffix)
                VALUES (:username, :base, :suffix)";

      $stmt = $this->_db->prepare($insert);
      $counter = 0;
      foreach ($usernames as $name) {
        $username = $name['username'];
        $base = rtrim($username, ' 0123456789');
        $suffix = (int) str_replace( $base, '', $username );

        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':base', $base);
        $stmt->bindParam(':suffix', $suffix);
        $stmt->execute();
        ++$counter;

        print("\r$counter of $recordTotal records processed.\r");
      }
    } catch (Exception $e) {
      die( 'ERROR: ' . print_r( $e, true) );
    }
    $this->_db->exec( "COMMIT TRANSACTION" );
    $this->_db = null;
  }

  public function recommend( $email ){
    $recommendation = '';
    $basename = explode( '@', $email )[0];
    $suffix = '';
    $nextTail = 0;
    $assigned = false;
    $this->_db->exec( "BEGIN TRANSACTION" );
    try {
      $sql = "select max(suffix)+1 as suffix from usernames where base=? ";
      $stmt = $this->_db->prepare( $sql );

    }catch( Exception $e ){
      die( 'Error: ' . print_r( $e, true) );
    }

    if (!$stmt) {
      die("DB Error: Please check if database is set up properly.\n ");
    }

    $users = $stmt->execute( array( $basename ) );
    $users = $stmt->fetchAll();

    $recommendation = $this->_processNext( $users, $basename );
    $i = count($users);
    echo "\n$i records.\n";
    $this->_db = null;
    return $recommendation;
    $this->_db->exec( "COMMIT TRANSACTION" );
  }

  protected function _processNext( $users, $basename ){
    foreach( $users as $user) {
      $newsuffix =  $user['suffix'];
      break;
    }
    return $basename . $newsuffix;
  }

  protected function _processAll( $users, $basename ){
    $i = 0;
    foreach( $users as $user ){
      if ( stristr( $user['username'], $basename ) !== FALSE ) {
        echo "\nUSERNAME: {$user['username']}";
       $tail = str_replace( $basename, '' , $user['username'] );
       if( ctype_digit( $tail ) ){
          $nextTail = (int) $suffix + 1;
          if ( $nextTail < $tail ) {
            $suffix = $nextTail;
            $assigned = true;
            break;
          }else{
            $suffix = $tail;
          }
       }
      }
      ++$i;
    }

    if( !$assigned && $suffix != '' ) {
      ++$suffix;
    }

    return $basename . $suffix;
  }
}



