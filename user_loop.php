<?php
/**
 * Opens the user data file as an array and loops through all records
 */

require('recommendation.php');

// get first parameter passed from commandline as email to be processed
$email = $argv[1];
echo "\n Starting process... \n\n";

$start  = microtime(true);
$usernames = file( 'usernames.txt' );
$recommendation = getRecommendation( $email, $usernames );
$end = microtime(true);
$lapsedTime = $end - $start;
echo "\nRecommended Username: $recommendation";
echo "\n\nProcessing Time: " . $lapsedTime;
echo "\n\n";
