USERNAME RECOMMENDATION SCRIPT
==============================

Requirements:
-------------
  PHP 5.4 & above, php-cli and php-sqlite module installed


## Objective:

  Build a script or program that will recommend username based on an email address provided.

## Installation:

  Copy the contents of the zip file to a directory where you can run PHP scripts from the commandline. php5-cli and php5-sqlite needs to be installed. On debian, you can run the following:
    sudo apt-get install php5-cli php5-sqlite


## Solutions & Implementations:

  There are three different PHP commandline scripts provided that make use of different strategies to come up with the same solution.

## 1. Array Looping - Slowest ##

### Description ###
The script saves the contents of the file into memory as an array. It then iterates over the entire array to match the email usernames already in use and determines the next best value. To improve performance, if a number is found to be skipped in the list of names, the skipped number will be used for the recommendation, and the loop execution ended.

### Files ###
    user_loop.php, recommendation.php
 
###  Commandline Syntax ###
    php user_loop.php john@email.com

### Average Execution Time to search ###
    for john@email.com: 2.5 - 3.0s


## 2. Regex Matching (Intermediate speed, slows down on bigger datasets) ##

### Description:
The base username from the provided email is used to create a regex pattern to retrieve matches against the username data file. The resulting array matches are processed to directly infer the next suitable username.
#### NOTE: #### an assumption here was made that the next suitable username would be derived from the maximum suffix value. For example, if we find that the last value for username john is "john999", then "john1000" would be recommended.

### Files:
    user_regex.php

### Commandline Syntax:
    php user_regex.php john@email.com


### Average Execution Time to search
    * for > 400000 and above records (eg. john@email.com) : 1.7 - 2.0s
    * for < 1000 records (eg. vince@email.com) : 0.4s


## 3. OOP / PDO_SQLite Implementation (Fastest) ##

### Description:
To leverage sqlite's ability to handle bigger data, the usernames in the provided text file needs to be saved to   a sqlite database. To initialize the database, the following command can be run:

    php user_oop.php dbsetup

The Username class provides methods to create the sqlite database and import the data from the text file. It is of note, however, that building the database takes around 30s. But the benefits in the end is apparent when it consistently outperforms the first two implementations especially when optimized with indexes.

This implementation seeks to minimize searching programmatically through the script instead passing it off to the database engine. As with the Regex Matching Implementation, an assumption was made to get only the
    highest index/suffix value and increment by 1 to get the recommended username.

### Files: ###
    user_oop.php, UsernameModel.php

### Commandline Syntax: ###
    
#### DATABASE SETUP:
      php user_oop.php dbsetup

#### GET RECOMMENDATION:
      php user_oop.php john@email.com

### Average Execution Time to search
    * for > 400000 records (eg. john@email.com) : 0.098s
    * for < 1000 records (eg. vince@email.com) : 0.0005 - 0.0007s